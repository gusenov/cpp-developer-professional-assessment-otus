#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <cassert>

void q1()
{
    class A {
    public:
        A() { std::cout << 'a'; }
        ~A() { std::cout << 'A'; }
    };
    
    class B {
    public:
        B() { std::cout << 'b'; }
        ~B() { std::cout << 'B'; }
        A a;
    };

    B b;
}
// abBA

// При конструировании сначала конструируются поля, потом вызывается деструктор.
// При деструкции сначала вызывается деструктор, потом происходит деструкция полей.
// Обратный порядок должен соблюдаться.

void q2() 
{
    int a = 10;
    int b = 20;
    int x;
    x = a, b;
    std::cout << x;
}
// 10

// Запятая может быть оператором или разделителем.
// Как разделитель используется в вызовах функций, определениях функций, объявлениях переменных, перечислений и подобных конструкций.

void q3()
{
    int a = 10;
    int b = 20;
    int x;
    x = (a, b);
    std::cout << x;
}
// 20

// Как бинарный оператор, она вычисляет первый операнд и отбрасывает результат,
// потом вычисляет второй операнд и возвращает это значение.
// У оператора запятой наименьший приоритет среди C операторов, 
// и она служит "точкой следования" - все что до уже вычислилось, все что после еще нет.

template <class T> void f(T) {
    using namespace std;

    static int i = 0;
    cout << ++i;
}

void q4()
{
    f(1);
    f(1.0);
    f(1);
}
// 112

void q5()
{
    class A {
    public:
        A() { std::cout << 'a'; }
        ~A() { std::cout << 'A'; }
    };

    class B : public A {
    public:
        B() { std::cout << 'b'; }
        ~B() { std::cout << 'B'; }
    };

    B b;
}
// abBA

namespace q13
{

void foo() {
    std::cout << "foo" << std::endl;
}

void bar() {
    std::thread t(foo);
    t.detach();  // отделяет thread of execution от thread object, позволяя дальнейшее независимое выполнение.
    // Если detach закоментировать, то программа аварийно завершится. 
}
// Вывод функции bar будет пустым.

}

namespace q14
{
    void foo() {
        std::cout << "foo" << std::endl;
    }

    void bar() {
        std::thread t(foo);
        t.detach();
        t.join();
        // программа аварийно завершится
        // terminate called after throwing an instance of 'std::system_error'
    }
}

namespace q15
{

void foo(int &s) {
    s = 42;
}

void bar() {
    int answer = 0;
    // std::thread t(foo, answer);
    
    // программа не скомпилируется
    // In instantiation of ‘std::thread::thread(_Callable&&, _Args&& ...) [with _Callable = void (&)(int&); _Args = {int&}; <template-parameter-1-3> = void]’:
    // error: static assertion failed: std::thread arguments must be invocable after conversion to rvalues

    // t.join();
    // std::cout << answer << std::endl;
}

}

namespace q16
{

std::mutex cout_mutex;

void foo() {
    using namespace std::chrono_literals;

    std::lock_guard<std::mutex> lock(cout_mutex);
    std::this_thread::sleep_for(1s);
    std::cout << "foo" << std::endl;
}

void bar() {
    std::thread t1(foo);
    foo();
    std::thread t2(foo);
    t1.join();
    t2.join();
}
// foo
// foo
// foo

}

bool q19(bool is_open, bool is_find)
{
    return (is_find && is_open) || !is_open;
}

// Если НАЙДЕНО то не имеет значения ОТКРЫТО или ЗАКРЫТО
// потому что если ЗАКРЫТО, то сработает в итоге !is_open.

template<typename F>
bool q19(F &&lambda)
{
    bool result = true;
    for (int is_open = 0; is_open <= 1; ++is_open)
    {
        for (int is_find = 0; is_find <= 1; ++is_find)
        {
            if (q19(is_open, is_find) != lambda(is_open, is_find))
                return false;
        }
    }
    return result;
}

int main()
{
    q13::bar();

    assert( q19([](bool is_open, bool is_find){ return ( is_find  || !is_open); }));
    assert(!q19([](bool is_open, bool is_find){ return ( is_find  && !is_open); }));
    assert(!q19([](bool is_open, bool is_find){ return (!is_find  && !is_open); }));
    assert(!q19([](bool is_open, bool is_find){ return ( is_find  ||  is_open); }));

    return 0;
}