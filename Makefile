all: assessment

assessment: main.o
	g++ -pthread main.o -o assessment

main.o: main.cc
	g++ -std=c++17 -c main.cc

clean:
	rm -rf *.o assessment

